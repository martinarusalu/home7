import java.util.*;

public class Puzzle {
	
	/**
	 * Kõik kasutatud tähed.
	 */
	private static List<Character> existingLetters = new ArrayList<Character>();
	
	/**
	 * Tähed, mis ei saa olla nullid (esitähed).
	 */
	private static List<Character> noZero = new ArrayList<Character>();
	
	/**
	 * Tähtede ja nende väärtuste paarid.
	 */
	private static HashMap<Character, Integer> letterValue = new HashMap<Character, Integer>();
	
	/**
	 * Lahenduste massiiv.
	 */
	private static List<String> solutions = new ArrayList<String>();
	
	/**
	 * Sõnade klassimuutujad (et meetoditest kergesti kätte saada).
	 */
	private static String a, b, c;

	/**
	 * Solve the word puzzle.
	 * 
	 * @param args three words (addend1, addend2 and sum)
	 */
	public static void main(String[] args) {
		args = new String []{"YKS", "KAKS", "KOLM"};
		checkAndEvaluate(args);
		setLetters();
		solvePuzzle();
	}
	
	/**
	 * Meetod, mis kontrollib sisendandmeid ja salvestab sõnad hilisemaks kasutamiseks
	 * @param args sõnad
	 */
	public static void checkAndEvaluate(String[] args) {
		if (args.length != 3) throw new RuntimeException("Nõutud on 3 argumenti.");
		
		if (args[0].length() < 2 || args[1].length() < 2 || args[2].length() < 2) {
			throw new RuntimeException("Sõna peab olema vähemalt 2 tähemärki.");
		}
		
		a = args[0].toUpperCase();
		b = args[1].toUpperCase();
		c = args[2].toUpperCase();
		
		// Sõna esimene täht ei saa olla null. Muidu algaks ka arv nulliga.
		noZero.add(a.charAt(0));
		noZero.add(b.charAt(0));
		noZero.add(c.charAt(0));
	}

	/**
	 * Algväärtustab kõik erinevad tähed.
	 */
	public static void setLetters() {
		for (int i = 0; i < a.length(); i++) {
			char addA = a.charAt(i);
			if (!letterValue.containsKey(addA)) { // Ainult unikaalsed
				existingLetters.add(addA); // Lisab tähtede listi
				letterValue.put(addA, -1); // -1 tähendab, et pole veel väärtust saanud.
			}
		}
		for (int i = 0; i < b.length(); i++) {
			char addB = b.charAt(i);
			if (!letterValue.containsKey(addB)) {
				existingLetters.add(addB);
				letterValue.put(addB, -1);
			}
		}
		for (int i = 0; i < c.length(); i++) {
			char addC = c.charAt(i);
			if (!letterValue.containsKey(addC)) {
				existingLetters.add(addC);
				letterValue.put(addC, -1);
			}
		}
	}
	
	/**
	 * Käivitab lahenduse, kuvab kasutajale tulemused.
	 */
	public static void solvePuzzle() {
		loopThrough(0); // Lahendusmeetod. Alustab esimesest tähest.
		System.out.println(a + " + " + b + " = " + c); // Lahendatav võrrand.
		if (solutions.size() < 1) System.out.println("Lahendused puuduvad");
		else System.out.println(solutions.size() + " lahendust:"); // Lahenduste arv.
		for (int i = 0; i < solutions.size(); i++) {
			System.out.println(solutions.get(i)); // Iga lahendus eraldi.
		}
	}
	
	/**
	 * Rekursiivne meetod, mis annab tähtedele väärtusi (0-9),
	 * arvestades seda, kas täht saab omada 0 väärtust ja kas see number on väärtustamiseks vaba
	 * (ehk ega ühelgi teisel tähel sama numbrit pole).
	 * 
	 * @param index mitmendat tähte väärtustada.
	 */
	public static void loopThrough (int index) {
		char letter = existingLetters.get(index); // võtab unikaalsetest tähtedes index kohal tähe.
		
		// Proovib erinevaid väärtusi anda (0-9)
		for (int i = noZero.contains(letter) ? 1 : 0; i < 10; i++) {
			
			// Kontrollib, ega eelnevatel tähtedel sellist numbrilist vastet pole.
			if (exists(i, index)) continue;
			
			// Kui vaba number leiti, antakse see tähele.
			letterValue.put(letter, i);
			
			// Kui on veel tähti, korratakse meetodit järgmise peal.
			if ((index+1) < existingLetters.size()) {
				loopThrough(index+1);
			}
			
			// Kui kõik tähed on numbri saanud, kontrollitakse, kas lahendus klapib.
			checkSolution();
		}
	}
	
	/**
	 * Kontrollib, ega eelnevatel tähtedel juba otsitavat väärtust poleks.
	 * 
	 * @param search otsitav väärtus
	 * @param index tähe indeks, millest eelnevate väärtusi vaadatakse.
	 * @return boolean kas selline number on juba mõnel eelneval tähel.
	 */
	public static boolean exists (int search, int index) {
		// Kontrollib ainult eelnevaid, sest järgnevaid hakatakse hiljem muutma.
		for (int i = 0; i < index; i++) {
			char letter = existingLetters.get(i); // kontrollitav täht
			int value = letterValue.get(letter); // kontrollitava tähe väärtus.
			if (value == search) {
				return true; // Sellise numbriga täht juba eksisteerib.
			}
		}
		return false; // Sellise numbriga tähte ei eksisteeri.
	}
	
	/**
	 * Annab tähtede järgi sõnadele väärtused,
	 * ning kontrollib, kas sellise kombinatsiooniga avaldis kehtib.
	 * Kui kehtib, lisatakse väärtused lahenduste massiivi.
	 */
	public static void checkSolution() {
		//Kontrollib, et kõik tähed oleks numbri saanud.
		if (letterValue.containsValue(-1)) return; // Kui mõni täht on väärtustamata.
		long numA = stringToNum(a); // Esimene liidetav
		long numB = stringToNum(b); // Teine liidetav
		long numC = stringToNum(c); // Summa
		if (numA + numB == numC) { // Kui avaldis on tõene.
			String s = numA + " + " + numB + " = " + numC;
			if (!solutions.contains(s)) solutions.add(s); // Lisab lahenduste massiivi.
		}
	}
	
	/**
	 * Võtab sisendiks sõna ja eelnevalt iga tähe kohta paigutab sõnasse numbri.
	 * Tekib arv.
	 * 
	 * @param toNum sõna, mille väärtust küsitakse.
	 * @return int sõna väärtus
	 */
	public static long stringToNum (String toNum) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < toNum.length(); i++) {
			sb.append(letterValue.get(toNum.charAt(i)));
		}
		return Long.valueOf(sb.toString());
	}
}
